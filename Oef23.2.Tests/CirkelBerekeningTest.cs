﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef23._2.Tests
{
    internal class CirkelBerekeningTest
    {
        [Test]
        public void OmtrekEnOppervlakte()
        {
            //Arrange
            int straal = 3;
            double omtrek;
            double oppervlakte;
            //Act
            omtrek = Math.Round((2 * straal * Math.PI), 2);
            oppervlakte = Math.Round(Math.PI * Math.Pow(straal, 2), 2);
            //Assert
            Assert.IsTrue(omtrek == 18.85 && oppervlakte == 28.27);
        }
    }
}
