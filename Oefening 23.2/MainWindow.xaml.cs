﻿using System;
using System.Media;
using System.Windows;

namespace Oefening_23._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnBereken_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lblOmtrek.Content = "Omtrek: ";
                lblOmtrek.Content += (2 * Math.PI * Convert.ToInt32(txtInput.Text)).ToString("0.00");
                lblOppervakte.Content = "Oppervlakte: ";
                lblOppervakte.Content += (Math.Pow(Convert.ToInt32(txtInput.Text), 2) * Math.PI).ToString("0.00");
            }
            catch (FormatException)
            {
                MessageBox.Show("U heeft geen numerieke waarde ingegeven.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
